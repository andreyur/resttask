import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pojo.PetModel;
import java.util.ArrayList;
import java.util.List;
import static io.restassured.RestAssured.given;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;

public class RestPetTest {
    private RequestSpecification requestSpecification;
    private PetModel testPet;
    private PetModel createdPet;
    private PetModel updatedPet;
    private List<Integer> requestId;
    public static final String PATH_REQUESTS_ID = "/pet/{petId}";
    public static final String PATH_PETS = "/pet";
    public static final String ID_PATH = "id";
    public static final String NAME_PATH = "name";
    public static final String STATUS_PATH = "status";

    @BeforeClass
    public void setUp() {
        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://petstore.swagger.io/v2")
                .setContentType(ContentType.JSON)
                .build();
        requestId = new ArrayList();
    }

    @Test(dataProvider = "CreatePet")
    public void postTest(String petName, int id) {
        testPet = new PetModel(null, petName, id, null, null, "available");
        //1 STEP Create Pet
        given()
                .log().all()
                .spec(requestSpecification)
                .basePath(PATH_PETS)
                .body(testPet)
                .when().post()
                .then()
                .assertThat().statusCode(HTTP_OK)
                .log().all()
                .body(ID_PATH, Matchers.equalTo(id))
                .body(NAME_PATH, Matchers.equalTo(petName))
                .body(STATUS_PATH, Matchers.equalTo(testPet.getStatus()));
        //2 STEP Get Pet By Id
        createdPet = given()
                .log().all()
                .spec(requestSpecification)
                .when().get(PATH_REQUESTS_ID, id)
                .then()
                .assertThat().statusCode(HTTP_OK)
                .log().all()
                .body(ID_PATH, Matchers.equalTo(testPet.getId()))
                .body(NAME_PATH, Matchers.equalTo(testPet.getName()))
                .body(STATUS_PATH, Matchers.equalTo(testPet.getStatus()))
                .extract().as(PetModel.class);
        requestId.add(createdPet.getId());
    }

    @Test(dataProvider = "UpdatePet")
    public void putTest(String petName, int id) {
        testPet = new PetModel(null, "Sveta", id, null, null, "available");
        updatedPet = new PetModel(null, petName, id, null, null, "sold");
        //1 STEP Create Pet
        given()
                .log().all()
                .spec(requestSpecification)
                .basePath(PATH_PETS)
                .body(testPet)
                .when().post()
                .then()
                .assertThat().statusCode(HTTP_OK);
        //2 STEP Update Pet
        given()
                .log().all()
                .spec(requestSpecification)
                .basePath(PATH_PETS)
                .body(updatedPet)
                .when().put()
                .then()
                .log().all()
                .assertThat().statusCode(HTTP_OK)
                .body(ID_PATH, Matchers.equalTo(updatedPet.getId()))
                .body(NAME_PATH, Matchers.equalTo(updatedPet.getName()))
                .body(STATUS_PATH, Matchers.equalTo(updatedPet.getStatus()));
        //3 STEP Get Pet By Id
        createdPet = given()
                .log().all()
                .spec(requestSpecification)
                .when().get(PATH_REQUESTS_ID, id)
                .then()
                .assertThat().statusCode(HTTP_OK)
                .log().all()
                .body(ID_PATH, Matchers.equalTo(updatedPet.getId()))
                .body(NAME_PATH, Matchers.equalTo(updatedPet.getName()))
                .body(STATUS_PATH, Matchers.equalTo(updatedPet.getStatus()))
                .extract().as(PetModel.class);
        requestId.add(createdPet.getId());

    }

    @Test
    public void deleteTest() {
        testPet = new PetModel(null, "Sveta", 124650, null, null, "available");
        //1 STEP Create Pet
        given()
                .log().all()
                .spec(requestSpecification)
                .basePath(PATH_PETS)
                .body(testPet)
                .when().post()
                .then()
                .assertThat().statusCode(HTTP_OK);
        //2 STEP Delete Pet
        given()
                .log().all()
                .spec(requestSpecification)
                .when().delete(PATH_REQUESTS_ID, testPet.getId())
                .then()
                .assertThat().statusCode(HTTP_OK);
        //3 STEP Get Pet By Id
        ValidatableResponse response = given()
                .log().all()
                .spec(requestSpecification)
                .when().get(PATH_REQUESTS_ID, testPet.getId())
                .then()
                .assertThat().statusCode(HTTP_NOT_FOUND)
                .log().all();
        if (response.extract().statusCode() == HTTP_OK) {
            createdPet = response.extract().as(PetModel.class);
            requestId.add(createdPet.getId());
        }
    }

    @DataProvider(name = "CreatePet")
    public static Object[][] createPet() {
        return new Object[][]{
                {"Sara", 201546},
                {"Ivan", 123546},
                {"Darina", 456546}
        };
    }

    @DataProvider(name = "UpdatePet")
    public static Object[][] updatePet() {
        return new Object[][]{
                {"Darina", 131546},
                {"Andrey", 433546},
                {"Sasha", 766546}
        };
    }

    @AfterClass
    public void tearDown() {
        requestId.forEach(requestId -> given().spec(requestSpecification).delete(PATH_REQUESTS_ID, requestId).then().log().all());
    }

}
